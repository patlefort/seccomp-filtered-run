seccomp-filtered-run
==============================

A simple program to run other programs with seccomp filters.

## Usage

```
# Run a program with filters
seccomp-filtered-run <filter filename> <program> [<args>...]

# Generate a compiled BPF program
cat <filter file> | seccomp-filtered-gen > <output file>
```

## Filters

Filter are specified in a JSON format and make use of [seccompiler](https://github.com/rust-vmm/seccompiler). Look at the [docs](https://github.com/rust-vmm/seccompiler/blob/main/docs/json_format.md) for documentation.
