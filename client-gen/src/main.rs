use seccompiler;
use std::convert::TryInto;
use std::io::prelude::Write;

fn main() {
	let filters: seccompiler::BpfMap = seccompiler::compile_from_json(
		std::io::stdin(),
		std::env::consts::ARCH.try_into().unwrap(),
	).unwrap();

	let mut bpf_prog_vec: Vec<u8> = Vec::new();

	for (_name, filter) in filters {
		bpf_prog_vec.clear();
		bpf_prog_vec.reserve(
			filter.len() *
			(
				std::mem::size_of_val(&filter[0].code) +
				std::mem::size_of_val(&filter[0].jt) +
				std::mem::size_of_val(&filter[0].jf) +
				std::mem::size_of_val(&filter[0].k)
			)
		);

		for entry in filter {
			bpf_prog_vec.extend(&entry.code.to_ne_bytes());
			bpf_prog_vec.extend(&entry.jt.to_ne_bytes());
			bpf_prog_vec.extend(&entry.jf.to_ne_bytes());
			bpf_prog_vec.extend(&entry.k.to_ne_bytes());
		}

		std::io::stdout().write(&bpf_prog_vec).unwrap();
	}
}
