use seccompiler;
use std::convert::TryInto;
use std::ffi::OsString;
use std::process::Command;

fn main() -> std::process::ExitCode {
	let args: Vec<OsString> = std::env::args_os().collect();

	let filters: seccompiler::BpfMap = seccompiler::compile_from_json(
		std::fs::File::open(&args[1]).unwrap(),
		std::env::consts::ARCH.try_into().unwrap(),
	).unwrap();

	for (_name, filter) in filters {
		seccompiler::apply_filter(&filter).unwrap();
	}

	let status = Command::new(&args[2])
		.args(&args[3..])
		.status()
		.expect("Command failed to start.");

	match status.code() {
		Some(code) => std::process::ExitCode::from(code as u8),
		None => std::process::ExitCode::SUCCESS
	}
}
